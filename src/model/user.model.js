const { DataTypes } = require("sequelize");

const createUserModel = (sequelize) => {
  return sequelize.define(
    "User",
    {
      userId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: "user_id",
      },
      fullName: {
        type: DataTypes.STRING,
        field: "full_name",
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        field: "email",
        unique: true,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        field: "password",
        allowNull: false,
      },
    },
    {
      tableName: "users",
      timestamps: false,
    }
  );
};
module.exports = {
  createUserModel,
};
