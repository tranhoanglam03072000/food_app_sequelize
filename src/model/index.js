const { Sequelize } = require("sequelize");
const {
  DB,
  USER,
  PASSWORD,
  HOST,
  dialect,
  PORT,
} = require("../config/db.config");
const { createRateResModel } = require("./rateRes.model");
const { createRestaurantModel } = require("./restaurant.model");
const { createUserModel } = require("./user.model");
const sequelize = new Sequelize(DB, USER, PASSWORD, {
  host: HOST,
  dialect: dialect,
  port: PORT,
});
const User = createUserModel(sequelize);
const Restaurant = createRestaurantModel(sequelize);

// User n -n Restaurant
User.belongsToMany(Restaurant, {
  as: "createRateResModel",
  through: createRateResModel,
  foreignKey: "userId",
});
Restaurant.belongsToMany(User, {
  as: "createRateResModel",
  through: createRateResModel,
  foreignKey: "resId",
});
module.exports = {
  sequelize,
};
