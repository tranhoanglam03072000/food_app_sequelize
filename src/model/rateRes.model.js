const { DataTypes, Sequelize } = require("sequelize");

const createRateResModel = (sequelize) => {
  return sequelize.define(
    "RateRes",
    {
      resId: {
        type: DataTypes.INTEGER,
        field: "res_id",
        allowNull: false,
      },
      userId: {
        type: DataTypes.INTEGER,
        field: "user_id",
        allowNull: false,
      },
      amount: {
        type: DataTypes.INTEGER,
        field: "amount",
        allowNull: false,
      },
      dateRate: {
        type: DataTypes.DATE,
        field: "date_rate",
        allowNull: false,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      },
    },
    {
      tableName: "rate_res",
      timestamps: false,
    }
  );
};
module.exports = {
  createRateResModel,
};
