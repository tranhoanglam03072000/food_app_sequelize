const { DataTypes } = require("sequelize");

const createRestaurantModel = (sequelize) => {
  return sequelize.define(
    "Restaurant",
    {
      resId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: "res_id",
      },
      resName: {
        type: DataTypes.STRING,
        field: "res_name",
        allowNull: false,
      },
      resImg: {
        type: DataTypes.STRING,
        field: "res_img",
        allowNull: false,
      },
      resDesc: {
        type: DataTypes.STRING,
        field: "res_desc",
        allowNull: false,
      },
    },
    {
      tableName: "restaurants",
      timestamps: false,
    }
  );
};
module.exports = {
  createRestaurantModel,
};
